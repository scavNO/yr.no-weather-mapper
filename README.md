Yr Weather Mapper
=================
This is my first attempt on writing an API.

Simple utility I primarily created for myself to provide me with
a list of yr.no weather data. It contains basic search and filter
methods, based on location and dates.

If it becomes useful for anyone else, or if you want to extend it, that is great.


Future plans
------------
As my needs expand, so will this project.

This includes

- More and better tests
- More dynamic features
- Filters
- Search
- Location based template search
- JSON return values

How to use
==========
You can request lists of Weather objects by either of the methods below.
Currently only List<Weather> is supported as return values, but more specific
return values will come.

I recommend mapping Weather objects to your own domain objects.

     private List<Weather> getWorld() {
         List<Weather> weatherList = new YrWeatherImpl().world("Sweden", "Stockholm");
     }

     private List<Weather> getNorway() {
         List<Weather> weatherList = new YrWeatherImpl().norway("Hordaland", "Arna");
     }

     private List<Weather> getNorwayByDate() {
         List<Weather> weatherList = new YrWeatherImpl().worldByDate("Sweden", "Stockholm", before, after);
     }


     private List<Weather> getWorldByList() {
         List<Weather> weatherList = new YrWeatherImpl().norwayByDate("Hordaland", "Arna", before, after);
     }


XML Data return values
----------------------
Currently you will receive one object or a list of objects
populated by data from <time> and nothing else. Again this was my
personal need. I may expand it in the future.

    <weatherdata>
        <forecast>
            <tabular>
                <time></time>
    <omitted closing tags>





