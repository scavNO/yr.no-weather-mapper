/*
 * Copyright (c) 2014, Dag Østgulen Heradstveit, dagherad@gmail.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *   1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *
 *   3. Neither the name of the copyright holder nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 *   EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *   OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *   THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 *   OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 *   TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *   EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.devbugger.yrweathermapper.groovy

import com.devbugger.yrweathermapper.YrImpl
import com.devbugger.yrweathermapper.domain.Weather
/**
 * Test class to test the end point of the API. This is where all the
 * user interaction will happen agains the API and the methods it exposes.
 * Any tests failing here are critical to the functionality of the
 * API and serves as a nice way to sum up if anything is wrong.
 */
class YrImplTest extends GroovyTestCase {

    Date before, after;

    /**
     * Called to set up the date objects for each test
     * method running here.
     */
    public void setUp() {
        this.before = Calendar.getInstance().getTime();
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 1);
        this.after = calendar.getTime();
    }

    /**
     * Test to fetch weather from a location somewhere in the world.
     * @throws Exception
     */
    void testWorldWeather() {
        List<Weather> weatherList = new YrImpl().getWorld("Sweden", "Stockholm");
        assert weatherList != null : "Failed to fetch weather for Stockholm, Sweden"

        weatherList.each { weather ->
            assert weather != null
            assert weather instanceof Weather
            assert weather.getFromDate().before(weather.getToDate()) : "FromDate is before ToDate"
            assert weather.getToDate().after(weather.getFromDate()) : "FromDate is after ToDate"
            assert "Stockholm" in weather.getLocation()
            assert "Sweden" in weather.getCountry()
        }
    }

    /**
     * Test to fetch weather from a location somewhere in the world based
     * on a start and end date.
     * @throws Exception
     */
    void testWorldWeatherFromToDate() {
        List<Weather> weatherList = new YrImpl().getWorldByDate("Sweden", "Stockholm", before, after);

        assert weatherList != null : "Does not contain any weather objects."

        weatherList.each { weather ->
            assert weather != null
            assert weather instanceof Weather
            assert weather.getFromDate().before(after) : "FromDate is before the requested date"
            assert weather.getToDate().after(before) : "ToDate is after the requested date"
            assert weather.getFromDate().before(weather.getToDate()) : "FromDate is before ToDate"
            assert weather.getToDate().after(weather.getFromDate()) : "FromDate is after ToDate"
            assert "Stockholm" in weather.getLocation()
            assert "Sweden" in weather.getCountry()
        }
    }

    /**
     * Test to fetch weather from a location in Norway.
     * @throws Exception
     */
    void testNorwayWeather() {
        List<Weather> weatherList = new YrImpl().getNorway("Hordaland", "Bergen");
        assert weatherList != null : "Failed to fetch weather for Bergen, Hordaland"

        weatherList.each { weather ->
            assert weather != null
            assert weather instanceof Weather
            assert weather.getFromDate().before(weather.getToDate()) : "FromDate is before ToDate"
            assert weather.getToDate().after(weather.getFromDate()) : "FromDate is after ToDate"
            assert "Bergen" in weather.getLocation()
            assert "Norway" in weather.getCountry()
        }
    }

    /**
     * Test to fetch weather from a location in Norway on a start and end date.
     * @throws Exception
     */
    void testNorwayWeatherFromToDate() {
        List<Weather> weatherList = new YrImpl().getNorwayByDate("Hordaland", "Bergen", before, after);

        assert weatherList != null : "Does not contain any weather objects."

        weatherList.each { weather ->
            assert weather != null
            assert weather instanceof Weather
            assert weather.getFromDate().before(after) : "FromDate is before the requested date"
            assert weather.getToDate().after(before) : "ToDate is after the requested date"
            assert weather.getFromDate().before(weather.getToDate()) : "FromDate is before ToDate"
            assert weather.getToDate().after(weather.getFromDate()) : "FromDate is after ToDate"
            assert "Bergen" in weather.getLocation()
            assert "Norway" in weather.getCountry()
        }
    }
}