/*
 * Copyright (c) 2014, Dag Østgulen Heradstveit, dagherad@gmail.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *   1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *
 *   3. Neither the name of the copyright holder nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 *   EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *   OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *   THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 *   OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 *   TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *   EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package parser
import com.devbugger.yrweathermapper.domain.Weather
import com.devbugger.yrweathermapper.location.LocationBuilder
import com.devbugger.yrweathermapper.parser.ParseWeather

class ParseWeatherTest extends GroovyTestCase {

    public void testGetWeatherFromXmlTest() {
        ParseWeather weatherXmlParser = new ParseWeather()
        List<Weather> weatherList = weatherXmlParser.get(new LocationBuilder()
                .setCountry("Norway")
                .setLocation("Bergen")
                .setUrl("http://www.yr.no/place/Norway/Hordaland/Bergen/Bergen/forecast.xml")
                .createLocation())

        assert weatherList != null
        assert !weatherList.isEmpty()

        Weather weather = weatherList.get(0)
        assert weather != null
        assert weather instanceof Weather
    }
}