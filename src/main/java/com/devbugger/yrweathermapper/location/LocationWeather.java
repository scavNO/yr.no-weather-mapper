/*
 * Copyright (c) 2014, Dag Østgulen Heradstveit, dagherad@gmail.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *   1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *
 *   3. Neither the name of the copyright holder nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 *   EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *   OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *   THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 *   OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 *   TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *   EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.devbugger.yrweathermapper.location;
import com.devbugger.yrweathermapper.domain.Location;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

final class LocationWeather {

    private final String documentLocation;
    private final LocationType locationType;

    public LocationWeather(LocationType locationType) {
        this.locationType = locationType;
        this.documentLocation = locationType.toString();
    }

    /**
     * Gets all the locations in the getWorld from http://fil.nrk.no/yr/viktigestader/verda.txt
     * This should be cached somewhere locally and should not
     * be used to spam servers at yr.no for data.
     * @return a @{link List} of {@link com.devbugger.yrweathermapper.domain.Location} objects or an empty {@link java.util.Collections.EmptyList}
     */
    public List<Location> getAll() {
        if(locationType.equals(LocationType.NORWAY)) {
            return createNorwayLocationList(LocationList.create(documentLocation));
        }

        if(locationType.equals(LocationType.WORLD)) {
            return createWorldLocationList(LocationList.create(documentLocation));
        }

        else {
            return Collections.emptyList();
        }
    }

    /**
     * From the current structure:
     * 3 = Location
     * 10 = Country
     * 17 = English XML url
     * @param documentLines each line in the text file representing a location
     * @return A populated array of {@link com.devbugger.yrweathermapper.domain.Location}
     */
    private List<Location> createWorldLocationList(List<String> documentLines) {
        List<Location> locations = new ArrayList<>();

        for(String textLine : documentLines) {
            String result[] = textLine.split("\\t");

            locations.add(new LocationBuilder()
                    .setCountryCode(result[0])
                    .setCountry(result[10])
                    .setLocation(result[3])
                    .setUrl(result[17])
                    .createLocation());
        }

        return locations;
    }

    private List<Location> createNorwayLocationList(List<String> documentLines) {
        List<Location> locations = new ArrayList<>();

        for(String textLine : documentLines) {
            String result[] = textLine.split("\\t");

            locations.add(new LocationBuilder()
                    .setCountryCode("NO")
                    .setCountry("Norway")
                    .setLocalArea(result[1])
                    .setLocation(result[7])
                    .setUrl(result[12])
                    .createLocation());
        }

        return locations;
    }
}