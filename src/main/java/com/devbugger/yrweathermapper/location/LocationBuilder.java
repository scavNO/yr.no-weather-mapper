/*
 * Copyright (c) 2014, Dag Østgulen Heradstveit, dagherad@gmail.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *   1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *
 *   3. Neither the name of the copyright holder nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 *   EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *   OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *   THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 *   OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 *   TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *   EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.devbugger.yrweathermapper.location;

import com.devbugger.yrweathermapper.domain.Location;

import java.util.Date;

final class LocationBuilder {
    private String country;
    private String location;
    private String localArea;
    private String url;
    private Date date;
    private String countryCode;

    public LocationBuilder setCountry(String country) {
        this.country = country;
        return this;
    }

    public LocationBuilder setLocation(String location) {
        this.location = location;
        return this;
    }

    public LocationBuilder setLocalArea(String localArea) {
        this.localArea = localArea;
        return this;
    }

    public LocationBuilder setUrl(String url) {
        this.url = url;
        return this;
    }

    public LocationBuilder setDate(Date date) {
        this.date = date;
        return this;
    }

    public LocationBuilder setCountryCode(String countryCode) {
        this.countryCode = countryCode;
        return this;
    }

    /**
     * Return the complete Location object from the builder.
     * @return {@link com.devbugger.yrweathermapper.domain.Location}
     */
    public Location createLocation() {
        return new Location(country, location, localArea, url, date, countryCode);
    }
}