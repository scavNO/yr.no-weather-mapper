/*
 * Copyright (c) 2014, Dag Østgulen Heradstveit, dagherad@gmail.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *   1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *
 *   3. Neither the name of the copyright holder nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 *   EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *   OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *   THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 *   OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 *   TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *   EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.devbugger.yrweathermapper.location;

import com.devbugger.yrweathermapper.domain.Location;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *  and proved access to this given list
 * Creates a list of all locations, while also providing access to all available {@link com.devbugger.yrweathermapper.domain.Location} based
 * on the given {@link com.devbugger.yrweathermapper.location.LocationType}.
 */
public final class LocationFind {

    private final Map<LocationKey, Location> locationQuery = new HashMap<>();
    private final List<Location> locationList;

    public LocationFind(LocationType locationType) {
        this.locationList = new LocationWeather(locationType).getAll();

        for(Location locationQuery : locationList) {
            if(locationType.equals(LocationType.NORWAY)) {
                this.locationQuery.put(new LocationKey(locationQuery.getLocation(), locationQuery.getLocalArea()), locationQuery);
            }

            if(locationType.equals(LocationType.WORLD)) {
                this.locationQuery.put(new LocationKey(locationQuery.getCountry(), locationQuery.getLocation()), locationQuery);
            }
        }
    }

    /**
     * Returns a list of all known locations for the given {@link LocationType}
     * @return
     */
    public List<Location> getLocationList() {
        return locationList;
    }

    /**
     * Creates a {@link LocationKey} for the {@link java.util.Map} based on a composite key.
     * It then fetches the object matching this key.
     * @param paramOne country or county/location
     * @param paramTwo localArea
     * @return the object that matches the key.
     */
    public Location get(String paramOne, String paramTwo) {
        LocationKey locationKey = new LocationKey(paramOne, paramTwo);
        return locationQuery.get(locationKey);
    }
}
