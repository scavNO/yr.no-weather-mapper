/*
 * Copyright (c) 2014, Dag Østgulen Heradstveit, dagherad@gmail.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *   1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *
 *   3. Neither the name of the copyright holder nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 *   EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *   OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *   THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 *   OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 *   TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *   EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.devbugger.yrweathermapper.location;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

final class LocationList {

    /**
     * Creates a @{link List} of @{link String} that contains each of the
     * forecast.xml files found on yr.no
     *
     * Returns an empty @{link List} if it fails.
     * @param resource
     * @return A list with each line of the document as one @{link String}.
     */
    public static List<String> create(String resource) {
        try(BufferedReader reader = new BufferedReader(new InputStreamReader(
            new URL(resource).openConnection().getInputStream()))) {

            List<String> documentLines = new ArrayList<>();
            String output;
            while((output = reader.readLine()) != null) {
                documentLines.add(output);
            }

            //Remove the top reference as this is only headers we wont use.
            documentLines.remove(0);
            return documentLines;

        } catch (IOException e) {
            System.err.println("Unable to create documentLines.");
            System.err.println(e.getCause().getMessage());

            return Collections.emptyList();
        }
    }
}