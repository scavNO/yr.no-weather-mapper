/*
 * Copyright (c) 2014, Dag Østgulen Heradstveit, dagherad@gmail.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *   1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *
 *   3. Neither the name of the copyright holder nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 *   EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *   OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *   THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 *   OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 *   TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *   EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.devbugger.yrweathermapper.parser;

import com.devbugger.yrweathermapper.domain.Weather;
import com.devbugger.yrweathermapper.domain.Location;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class ParseWeather {

    public List<Weather> get(Location location) {
        return parse(location);
    }

    private List<Weather> parse(Location location) {
        List<Weather> weatherList = new ArrayList<>();
        Weather weather;

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(new URL(location.getUrl()).openConnection().getInputStream());
            document.getDocumentElement().normalize();

            NodeList nodes = document.getElementsByTagName(XmlTag.ROOT_ELEMENT.toString());
            Node node = nodes.item(0);

            NodeList timeNodeList = node.getChildNodes();
            for(int i = 0; i < timeNodeList.getLength(); i++) {
                if(timeNodeList.item(i).getNodeType() == Node.ELEMENT_NODE) {
                    Element timeElement = (Element) timeNodeList.item(i);
                    weather = new Weather();
                    weather.setId(i);

                    if(location.getLocalArea() != null) {
                        weather.setLocation(location.getLocalArea());
                    }
                    else {
                        weather.setLocation(location.getLocation());
                    }


                    //Check to make sure that we do not try to add a
                    //country to a norwegian weather object.
                    if(location.getCountry() != null) {
                        weather.setCountry(location.getCountry());
                    }

                    weather.setFromDate(DateXmlFormatter.date(timeElement.getAttribute(
                            XmlTag.FROM_DATE.toString())));
                    weather.setToDate(DateXmlFormatter.date(timeElement.getAttribute(
                            XmlTag.TO_DATE.toString())));

                    weatherList.add(completeChildNodes(
                            timeElement.getChildNodes(), weather));
                }
            }

        } catch (IOException | ParserConfigurationException | SAXException e) {
            System.err.println("Unable to parse document.");
            System.err.println("The resource supplied was: " + location.getUrl());
            System.err.println(e.getCause().getMessage());

            return Collections.emptyList();
        }

        return weatherList;
    }

    /**
     * Complete the child nodes from the current XML document.
     * @param childNodeList a node containing an actual weather report
     * @param weather the returned weather object create from the node
     * @return Current {@link com.devbugger.yrweathermapper.domain.Weather} object
     */
    private Weather completeChildNodes(NodeList childNodeList, Weather weather) {
        for(int j = 0; j < childNodeList.getLength(); j++) {
            if(childNodeList.item(j).getNodeType() == Node.ELEMENT_NODE) {
                Element childElement = (Element) childNodeList.item(j);

                String tag = childElement.getTagName();

                if(tag.equals(XmlTag.WEATHER_SYMBOL_TAG.toString())) {
                    weather.setSymbol(childElement.getAttribute(
                            XmlTag.WEATHER_SYMBOL_VAR.toString()));
                    weather.setWeatherName(childElement.getAttribute(
                            XmlTag.WEATHER_SYMBOL_NAME.toString()));
                }

                if(tag.equals(XmlTag.TEMPERATURE_TAG.toString())) {
                    weather.setTemperature(Integer.valueOf(childElement.getAttribute(
                            XmlTag.TEMPERATURE_VALUE.toString())));
                }

                if(tag.equals(XmlTag.WIND_DIRECTION_TAG.toString())) {
                    weather.setWindDirectionCode(childElement.getAttribute(
                            XmlTag.WIND_DIRECTION_CODE.toString()));
                    weather.setWindDirectionText(childElement.getAttribute(
                            XmlTag.WIND_DIRECTION_NAME.toString()));
                }

                if(tag.equals(XmlTag.WIND_SPEED_TAG.toString())) {
                    weather.setWindSpeed(Double.valueOf(childElement.getAttribute(
                            XmlTag.WIND_SPEED.toString())));
                    weather.setWindTypeName(childElement.getAttribute(
                            XmlTag.WIND_TYPE_NAME.toString()));
                }
            }
        }

        return weather;
    }
}