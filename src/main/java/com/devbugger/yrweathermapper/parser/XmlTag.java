/*
 * Copyright (c) 2014, Dag Østgulen Heradstveit, dagherad@gmail.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *   1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *
 *   3. Neither the name of the copyright holder nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 *   EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *   OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *   THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 *   OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 *   TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *   EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.devbugger.yrweathermapper.parser;

/**
 * Enum class that also provides default text
 * that will defines all XML tags in yr.no.
 *
 * These are mapping the currently active tags that is usable
 * by the parser in @{link ParseWeather} and which needs to be guaranteed
 * not to contain any spelling errors as this is fatal for the execution.
 */
enum XmlTag {
    ROOT_ELEMENT("tabular"),
    FROM_DATE("from"),
    TO_DATE("to"),
    WEATHER_SYMBOL_TAG("symbol"),
    WEATHER_SYMBOL_NAME("name"),
    WEATHER_SYMBOL_VAR("var"),
    TEMPERATURE_TAG("temperature"),
    TEMPERATURE_VALUE("value"),
    WIND_DIRECTION_TAG("windDirection"),
    WIND_DIRECTION_CODE("code"),
    WIND_DIRECTION_NAME("name"),
    WIND_SPEED_TAG("windSpeed"),
    WIND_SPEED("mps"),
    WIND_TYPE_NAME("name");

    private final String code;

    private XmlTag(final String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return code;
    }
}