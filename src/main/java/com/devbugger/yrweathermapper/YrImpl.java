/*
 * Copyright (c) 2014, Dag Østgulen Heradstveit, dagherad@gmail.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *   1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *
 *   3. Neither the name of the copyright holder nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 *   EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *   OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *   THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 *   OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 *   TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *   EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.devbugger.yrweathermapper;

import com.devbugger.yrweathermapper.domain.Weather;
import com.devbugger.yrweathermapper.domain.Location;
import com.devbugger.yrweathermapper.location.LocationFind;
import com.devbugger.yrweathermapper.parser.ParseWeather;
import com.devbugger.yrweathermapper.location.LocationType;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

public final class YrImpl implements Yr {

    @Override
    public List<Location> getLocationByLocationType(LocationType locationType) {
        return new LocationFind(locationType).getLocationList();
    }

    @Override
    public List<Weather> getWorld(String country, String location) {
        Location locationQuery =  new LocationFind(LocationType.WORLD).get(country, location);
        return new ParseWeather().get(locationQuery);
    }

    @Override
    public List<Weather> getWorldByDate(String country, String location, Date before, Date after) {
        Location locationQuery =  new LocationFind(LocationType.WORLD).get(country, location);
        List<Weather> weatherList = new ParseWeather().get(locationQuery);

        return removeItems(weatherList, before, after);
    }

    @Override
    public List<Weather> getNorway(String location, String localArea) {
        Location norwayQuery =  new LocationFind(LocationType.NORWAY).get(location, localArea);
        return new ParseWeather().get(norwayQuery);
    }

    @Override
    public List<Weather> getNorwayByDate(String location, String localArea, Date before, Date after) {
        Location norwayQuery =  new LocationFind(LocationType.NORWAY).get(location, localArea);
        List<Weather> weatherList = new ParseWeather().get(norwayQuery);

        return removeItems(weatherList, before, after);
    }

    /**
     * Remove any elements that does not fit the date pattern from the method.
     * This method can be called when ever items needs to be filtered by date.
     * It is okay to have a mutable {@link java.util.List} here as it will not live past execution
     * of this specific method.
     * @param weatherList the list of {@link com.devbugger.yrweathermapper.domain.Weather} objects
     * @param before start date for the requested weather reports
     * @param after end date for the requested weather reports
     * @return List of @{link Weather}
     */
    private List<Weather> removeItems(List<Weather> weatherList, Date before, Date after) {
        for(Iterator<Weather> iterator = weatherList.iterator(); iterator.hasNext();) {
            Weather weather = iterator.next();
            if(!weather.getFromDate().before(after) && weather.getToDate().after(before)) {
                iterator.remove();
            }
        }

        return weatherList;
    }
}