/*
 * Copyright (c) 2014, Dag Østgulen Heradstveit, dagherad@gmail.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *   1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *
 *   3. Neither the name of the copyright holder nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 *   EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *   OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *   THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 *   OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 *   TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *   EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.devbugger.yrweathermapper;

import com.devbugger.yrweathermapper.domain.Weather;
import com.devbugger.yrweathermapper.domain.Location;
import com.devbugger.yrweathermapper.location.LocationType;

import java.util.Date;
import java.util.List;

/**
 * !!! By using this application, you accept the license put forth by YR.no !!!
 * !!!      I am not responsible for failures to follow this license        !!!
 * !!! By using this application, you accept the license put forth by YR.no !!!
 * !!!      I am not responsible for failures to follow this license        !!!
 * !!! By using this application, you accept the license put forth by YR.no !!!
 * !!!      I am not responsible for failures to follow this license        !!!
 *
 * Fetch weather from yr.no from either Norwegian or international sources
 * but eventually providing the same @{link List} of @{link Weather} objects for use within
 * any application.
 *
 * Locations defined in {@link com.devbugger.yrweathermapper.location.LocationType}
 *  - Norwegian -> http://fil.nrk.no/yr/viktigestader/noreg.txt
 *  - International -> http://fil.nrk.no/yr/viktigestader/verda.txt
 *
 *  @author Dag Østgulen Heradsveit
 */
public interface Yr {

    /**
     * Returns all the possible locations that can be parsed.
     * @param locationType can be either WORLD or NORWAY
     * @return a {@link java.util.List} of {@link com.devbugger.yrweathermapper.domain.Location}
     */
    public List<Location> getLocationByLocationType(LocationType locationType);

    /**
     * Get a list of a weather report from a specific place somewhere
     * in the getWorld by country and location. E.g. getWorld("Sweden", "Stockholm").
     * @param country the country to fetch from
     * @param location the location with the given country
     * @return a {@link java.util.List} of {@link com.devbugger.yrweathermapper.domain.Weather}
     */
    public List<Weather> getWorld(String country, String location);

    /**
     * Get a @{link List} of @{link Weather} that is within the boundaries
     * of the Weather objects fromDate and afterDate in the getWorld by country
     * and location. E.g. getWorld("Sweden", "Stockholm").
     * @param country the country to fetch from
     * @param location the location with the given country
     * @param before start date for the requested weather reports
     * @param after  end date for the requested weather reports
     * @return a {@link java.util.List} of {@link com.devbugger.yrweathermapper.domain.Weather}
     */
    public List<Weather> getWorldByDate(String country, String location, Date before, Date after);


    /**
     * Get a list of a weather report from a specific place somewhere
     * in Norway by county and localArea. E.g. getNorway("Hordaland", "Bergen");
     * @param location the county of this weather report
     * @param localArea the area within the given county
     * @return a {@link java.util.List} of {@link com.devbugger.yrweathermapper.domain.Weather}
     */
    public List<Weather> getNorway(String location, String localArea);

    /**
     * Get a @{link List} of @{link Weather} that is within the boundaries
     * of the Weather objects fromDate and afterDate in Norway by county and
     * localArea. E.g. getNorway("Hordaland", "Bergen").
     * @param location the county of this weather report
     * @param localArea the area within the given county
     * @param before start date for the requested weather reports
     * @param after end date for the requested weather reports
     * @return  a {@link java.util.List} of {@link com.devbugger.yrweathermapper.domain.Weather}
     */
    public List<Weather> getNorwayByDate(String location, String localArea, Date before, Date after);

}
