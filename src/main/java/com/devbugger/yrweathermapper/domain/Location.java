/*
 * Copyright (c) 2014, Dag Østgulen Heradstveit, dagherad@gmail.com
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *   1. Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *
 *   3. Neither the name of the copyright holder nor the names of its contributors
 *      may be used to endorse or promote products derived from this software without
 *      specific prior written permission.
 *
 *   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 *   EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 *   OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
 *   THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *   SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 *   OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 *   HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 *   TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *   EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.devbugger.yrweathermapper.domain;

import java.util.Date;

/**
 * A location parsed from the {@link com.devbugger.yrweathermapper.location.LocationWeather} class.
 * It contains all the necessary data to perform a search on a weather location using
 * {@link com.devbugger.yrweathermapper.parser.ParseWeather}
 */
public final class Location {

    private final String country;
    private final String location;
    private final String localArea;
    private final String url;
    private final Date date;
    private final String countryCode;

    public Location(String country, String location, String localArea, String url, Date date, String countryCode) {
        this.country = country;
        this.location = location;
        this.localArea = localArea;
        this.url = url;
        this.date = date;
        this.countryCode = countryCode;
    }

    public String getCountry() {
        return country;
    }

    public String getLocation() {
        return location;
    }


    public String getLocalArea() {
        return localArea;
    }

    public String getUrl() {
        return url;
    }

    public Date getDate() {
        return date;
    }

    public String getCountryCode() {
        return countryCode;
    }

    @Override
    public String toString() {
        return "Location{" +
                "country='" + country + '\'' +
                ", location='" + location + '\'' +
                ", localArea='" + localArea + '\'' +
                ", url='" + url + '\'' +
                ", date=" + date +
                ", countryCode='" + countryCode + '\'' +
                '}';
    }
}